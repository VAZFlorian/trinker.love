module.exports = {
    title: function () {
        return `
$$$$$$$$\\        $$\\           $$\\                           $$\\                               
\\__$$  __|       \\__|          $$ |                          $$ |                              
   $$ | $$$$$$\\  $$\\ $$$$$$$\\  $$ |  $$\\  $$$$$$\\   $$$$$$\\  $$ | $$$$$$\\ $$\\    $$\\  $$$$$$\\  
   $$ |$$  __$$\\ $$ |$$  __$$\\ $$ | $$  |$$  __$$\\ $$  __$$\\ $$ |$$  __$$\\\\$$\\  $$  |$$  __$$\\ 
   $$ |$$ |  \\__|$$ |$$ |  $$ |$$$$$$  / $$$$$$$$ |$$ |  \\__|$$ |$$ /  $$ |\\$$\\$$  / $$$$$$$$ |
   $$ |$$ |      $$ |$$ |  $$ |$$  _$$<  $$   ____|$$ |      $$ |$$ |  $$ | \\$$$  /  $$   ____|
   $$ |$$ |      $$ |$$ |  $$ |$$ | \\$$\\ \\$$$$$$$\\ $$ |$$\\   $$ |\\$$$$$$  |  \\$  /   \\$$$$$$$\\ 
   \\__|\\__|      \\__|\\__|  \\__|\\__|  \\__| \\_______|\\__|\\__|  \\__| \\______/    \\_/     \\_______|
================================================================================================
   `.yellow;
    },

    line: function (title = "=") {
        return title.padStart(48, "=").padEnd(96, "=").bgBrightYellow.black;
    },

    allMale: function (p) {
        let list = [];
        for (let i of p) {
            if (i.gender === "Male") {
                list.push(i);
            }
        }
        return n;
    },

    allFemale: function (p) {
        let list = [];
        for (let i of p) {
            if (i.gender === "Female") {
                list.push(i);
            }
        }
        return n;
    },

    nbOfMale: function (p) {
        let n = 0;
        for (let i of p) {
            if (i.gender === "Male") {
                n++;
            }
        }
        return n;
    },

    nbOfFemale: function (p) {
        let n = 0;
        for (let i of p) {
            if (i.gender === "Female") {
                n++;
            }
        }
        return n;
    },

    nbOfMaleInterest: function (p) {
        let n = 0;
        for (let i of p) {
            if (i.looking_for === "M") {
                n++;
            }
        }
        return n;
    },

    nbOfFemaleInterest: function (p) {
        let n = 0;
        for (let i of p) {
            if (i.looking_for === "F") {
                n++;
            }
        }
        return n;
    },

    dollar2000_mini: function (p) {
        let n = 0;
        let value = 0;
        for (let i of p) {
            result = i.income;
            value = parseFloat(result.slice(1));
            if (value > 2000) {
                n++;
            }
        }
        return n;
    },

    likedrama: function (p) {
        let n = 0;
        for (let i of p) {
            if (i.pref_movie === "Drama") {
                n++;
            }
        }
        return n;
    },

    scifemale: function (p) {
        let n = 0;
        for (let i of p) {
            if (i.gender === "Female" && i.pref_movie.includes("Sci-Fi")) {
                n++;
            }
        }
        return n;
    },

    doc1482: function (p) {
        let n = 0;
        let value = 0;
        for (let i of p) {
            result = i.income;
            value = parseFloat(result.slice(1));
            if (value > 1482 && i.pref_movie.includes("Documentary")) {
                n++;
            }
        }
        return n;
    },

    // faire avec console.log sans classe ni objets

    list4000: function (p) {
        let n = 0;
        let value = 0;
        let retourne = [];
        class profil {
            constructor(nom, prénom, id, revenu) {
                this.nom = nom;
                this.prénom = prénom;
                this.id = id;
                this.revenu = revenu;
            }
        }

        for (let i of p) {
            result = i.income;
            value = parseFloat(result.slice(1));
            if (value > 4000) {
                let maligne = new profil(i.first_name, i.last_name, i.id, i.income);
                retourne.push(maligne);
            }
        }
        return retourne;
    },

    plusriche: function (p) {
        let n = 0;
        var value = 0;
        var plusgranderichesse = 0;
        var id_richesse = 0;
        let name_richesse = "";
        for (let i of p) {
            result = i.income;
            value = parseFloat(result.slice(1));
            if (value > plusgranderichesse && i.gender == "Male") {
                plusgranderichesse = value;
                id_richesse = i.id;
                name_richesse = i.last_name;
            }
        }
        return (
            "id : " +
            id_richesse +
            " nom : " +
            name_richesse +
            " richesse : " +
            plusgranderichesse +
            "$"
        );
    },

    salairemoyen: function (p) {
        let n = 0;
        var value = 0;
        var total = 0;
        for (let i of p) {
            result = i.income;
            value = parseFloat(result.slice(1));
            total += value;
            n++;
        }

        return total / n;
    },

    salairemedian: function (p) {
        let n = 0;
        var value = 0;
        var liste = [];
        for (let i of p) {
            result = i.income;
            value = parseFloat(result.slice(1));
            liste.push(value);
            n++;
        }
        var listetrié = liste.sort(function (a, b) {
            return a - b;
        });

        if (n % 2 == 1) {
            return listetrié[Math.trunc(n / 2)];
        }
        return (listetrié[n / 2] + listetrié[n / 2 - 1]) / 2;
    },

    hemisnord: function (p) {
        let n = 0;
        for (let i of p) {
            if (i.latitude > 0) {
                n++;
            }
        }
        return n;
    },

    hemissud: function (p) {
        let n = 0;
        let sum = 0;
        let result = 0;
        let value = 0;
        for (let i of p) {
            if (i.latitude < 0) {
                n++;
                result = i.income;
                value = parseFloat(result.slice(1));
                sum += value;
            }
        }
        return parseFloat(sum / n);
    },

    distance: function (gpslati, gpslongi, userlati, userlongi) {
        return Math.sqrt((userlati - gpslati) ** 2 + (userlongi - gpslongi) ** 2);
    },



    berenice: function (p) {
        let n = 0;
        let sum = 0;
        let berelati = 15.5900396;
        let berelongi = -87.879523;
        let userlati = 0;
        let userlongi = 0;
        let distance_user = 0;
        let plus_proche = -1;
        let plus_proche_id = 0;
        let plus_proche_last_name = "";
        for (let i of p) {
            userlati = i.latitude;
            userlongi = i.longitude;
            distance_user = this.distance(berelati, berelongi, userlati, userlongi);
            if (
                distance_user != 0 &&
                (distance_user < plus_proche || plus_proche < 0)
            ) {
                plus_proche_id = i.id;
                plus_proche_last_name = i.last_name;
                plus_proche = distance_user;
            }
        }
        let retourne =
            "Monsieur ou Madame " +
            plus_proche_last_name +
            " avec l'id : " +
            plus_proche_id +
            " est le plus proche de Bérénice Cawt";
        return retourne;
    },

    ruibrach: function (p) {
        let n = 0;
        let sum = 0;
        let ruilati = 33.347316;
        let ruilongi = 120.16366;
        let userlati = 0;
        let userlongi = 0;
        let distance_user = 0;
        let plus_proche = -1;
        let plus_proche_id = 0;
        let plus_proche_last_name = "";
        for (let i of p) {
            userlati = i.latitude;
            userlongi = i.longitude;
            distance_user = this.distance(ruilati, ruilongi, userlati, userlongi);
            if (
                distance_user != 0 &&
                (distance_user < plus_proche || plus_proche < 0)
            ) {
                plus_proche_id = i.id;
                plus_proche_last_name = i.last_name;
                plus_proche = distance_user;
            }
        }
        let retourne =
            "Monsieur ou Madame " +
            plus_proche_last_name +
            " avec l'id : " +
            plus_proche_id +
            " est le plus proche de Bérénice Cawt";
        return retourne;
    },



    boshard: function(p,last_name = "Boshard", first_name = "Josée", nb_plus_proches=10){
  
    for (let i of p) {
        if (i.last_name == last_name && i.first_name == first_name){
            var boshardlati=i.latitude;
            var boshardlongi=i.longitude;
        }
    }
        let n = 0;
        let sum = 0;

        let userlati = 0;
        let userlongi = 0;
        let distance_user = 0;
        let listeuser = [];

        class profil {
            constructor(last_name, id, distance) {
                this.last_name = last_name;
                this.id = id;
                this.distance = distance;
            }
        }

        for (let i of p) {
            userlati = i.latitude;
            userlongi = i.longitude;
            distance_user = this.distance(
                boshardlati,
                boshardlongi,
                userlati,
                userlongi
            );
            let maligne = new profil(i.last_name, i.id, distance_user);
            listeuser.push(maligne);
        }
        let listetrié = listeuser.sort(function (a, b) {
            return b.distance - a.distance;
        });
        let retourne = [];
        for (let pas = 1; pas < nb_plus_proches + 1; pas++) {
            retourne.push(listetrié[listetrié.length - pas]);
        }
        return retourne;
    },

    google: function (p) {
        let listeuser = [];
        class profil {
            constructor(last_name, id) {
                this.last_name = last_name;
                this.id = id;
            }
        }
        for (let i of p) {
            result = i.income;
            value = parseFloat(result.slice(1));
            if (i.email.includes("google")) {
                let maligne = new profil(i.last_name, i.id);
                listeuser.push(maligne);
            }
        }
        return listeuser;
    },

    plusagée: function (p) {
        let n = 0;
        var value = 0;
        let pluspetiteannée = 2200;
        let pluspetitmois = 15;
        let pluspetitjour = 50;
        var id_age = 0;
        let name_age = "";
        for (let i of p) {
            result = i.date_of_birth;
            let valueannée = parseFloat(result.slice(0, 4));
            if (valueannée < pluspetiteannée) {
                pluspetiteannée = valueannée;
            }
        }

        for (let i of p) {
            result = i.date_of_birth;
            let valueannée = parseFloat(result.slice(0, 4));
            let valuemois = parseFloat(result.slice(5, 7));
            if (valuemois < pluspetitmois && valueannée == pluspetiteannée) {
                pluspetitmois = valuemois;
            }
        }
        let date_age = "";
        for (let i of p) {
            result = i.date_of_birth;
            let valueannée = parseFloat(result.slice(0, 4));
            let valuemois = parseFloat(result.slice(5, 7));
            let valuejour = parseFloat(result.slice(8));

            if (
                valuejour < pluspetitjour &&
                valueannée == pluspetiteannée &&
                valuemois == pluspetitmois
            ) {
                pluspetitjour = valuejour;
                id_age = i.id;
                name_age = i.last_name;
                date_age = i.date_of_birth;
            }
        }
        let retourne =
            "Monsieur ou Madame " +
            name_age +
            " avec l'id : " +
            id_age +
            " est la personne la plus agée, né(e) en " +
            date_age;
        return retourne;
    },

    plusjeune: function (p) {
        let n = 0;
        var value = 0;
        let pluspetiteannée = -1;
        let pluspetitmois = -1;
        let pluspetitjour = -1;
        var id_age = 0;
        let name_age = "";
        for (let i of p) {
            result = i.date_of_birth;
            let valueannée = parseFloat(result.slice(0, 4));
            if (valueannée > pluspetiteannée) {
                pluspetiteannée = valueannée;
            }
        }

        for (let i of p) {
            result = i.date_of_birth;
            let valueannée = parseFloat(result.slice(0, 4));
            let valuemois = parseFloat(result.slice(5, 7));
            if (valuemois > pluspetitmois && valueannée == pluspetiteannée) {
                pluspetitmois = valuemois;
            }
        }
        let date_age = "";
        for (let i of p) {
            result = i.date_of_birth;
            let valueannée = parseFloat(result.slice(0, 4));
            let valuemois = parseFloat(result.slice(5, 7));
            let valuejour = parseFloat(result.slice(8));

            if (
                valuejour > pluspetitjour &&
                valueannée == pluspetiteannée &&
                valuemois == pluspetitmois
            ) {
                pluspetitjour = valuejour;
                id_age = i.id;
                name_age = i.last_name;
                date_age = i.date_of_birth;
            }
        }
        let retourne =
            "Monsieur ou Madame " +
            name_age +
            " avec l'id : " +
            id_age +
            " est la personne la plus jeune, né(e) en " +
            date_age;
        return retourne;
    },

    returnage: function (valueannée, valuemois, valuejour) {
        let d = new Date();
        let age = 0;
        let jour_actuel = d.getUTCDate();
        let mois_actuel = d.getUTCMonth();
        let année_actuel = d.getUTCFullYear();
        if (valuemois < mois_actuel) {
            age = année_actuel - valueannée;
        } else if (valuemois == mois_actuel && valuejour < jour_actuel) {
            age = année_actuel - valueannée;
        } else {
            age = année_actuel - valueannée - 1;
        }
        return age;
    },

    differenceage: function (p) {
        let cpt = 0;
        let somme = 0;
        for (let i of p) {
            let resulti = i.date_of_birth;
            let année_de_i = parseFloat(resulti.slice(0, 4));
            let mois_de_i = parseFloat(resulti.slice(5, 7));
            let jour_de_i = parseFloat(resulti.slice(8));
            for (let j of p) {
                if (i.id < j.id) {
                    resultj = j.date_of_birth;
                    let année_de_j = parseFloat(resultj.slice(0, 4));
                    let mois_de_j = parseFloat(resultj.slice(5, 7));
                    let jour_de_j = parseFloat(resultj.slice(8));
                    let age_i = this.returnage(année_de_i, mois_de_i, jour_de_i);
                    let age_j = this.returnage(année_de_j, mois_de_j, jour_de_j);
                    somme += Math.abs(age_j - age_i);
                    cpt++;
                }
            }
        }
        return somme / cpt;
    },

    liste_genres: function (p) {
        ma_liste = new Set();
        for (let i of p) {
            let liste_genre_i = i.pref_movie.split("|");
            for (let genre of liste_genre_i) {
                ma_liste.add(genre);
            }
        }
        let retourne = Array.from(ma_liste);
        return retourne;
    },

    filmpopulaire: function (p) {
        let cpt_max = 0;
        let result = "";
        var liste_genre_film = this.liste_genres(p);
        console.log(liste_genre_film);

        for (let i of liste_genre_film) {
            let cpt = 0;
            for (let j of p) {
                if (j.pref_movie.includes(i)) {
                    cpt++;
                }

                if (cpt > cpt_max) {
                    cpt_max = cpt;
                    result = i;
                }
            }
        }
        return result;
    },

    ordrefilmpopulaire: function (p) {
        var liste_genre_film = this.liste_genres(p);

        var genre_popularité = liste_genre_film.sort(function (a, b) {
            cpt_a = 0;
            cpt_b = 0;
            for (let j of p) {
                if (j.pref_movie.includes(a)) {
                    cpt_a++;
                }
                if (j.pref_movie.includes(b)) {
                    cpt_b++;
                }
            }
            return cpt_b - cpt_a;
        });

        return genre_popularité;
    },

    ordrefilmpopulaire2: function (p) {
        var liste_genre_film = this.liste_genres(p);
        var genre_popularité = [];

        for (let i of liste_genre_film) {
            let cpt = 0;
            for (let j of p) {
                if (j.pref_movie.includes(i)) {
                    cpt++;
                }
                genre_popularité[i] = cpt;
            }
        }

        return genre_popularité;
    },

    moyennefilmnoirhomme: function (p) {
        let total = 0;

        let age = 0;
        let cpt = 0;
        for (let i of p) {
            if (i.pref_movie.includes("Film-Noir") && i.gender == "Male") {
                cpt++;
                result = i.date_of_birth;
                let valueannée = parseFloat(result.slice(0, 4));
                let valuemois = parseFloat(result.slice(5, 7));
                let valuejour = parseFloat(result.slice(8));

                let d = new Date();
                let jour_actuel = d.getUTCDate();
                let mois_actuel = d.getUTCMonth();
                let année_actuel = d.getUTCFullYear();
                if (valuemois < mois_actuel) {
                    age = année_actuel - valueannée;
                } else if (valuemois == mois_actuel && valuejour < jour_actuel) {
                    age = année_actuel - valueannée;
                } else {
                    age = année_actuel - valueannée - 1;
                }

                total += age;
            }
        }

        return total / cpt;
    },

    salairemoyenhomme: function (p) {
        let n = 0;
        var value = 0;
        var total = 0;
        for (let i of p) {
            if (i.gender == "Male") {
                result = i.income;
                value = parseFloat(result.slice(1));
                total += value;
                n++;
            }
        }

        return total / n;
    },

    moyennefilmnoirfemmepauvreparis: function (p) {
        let total = 0;

        let age = 0;
        let cpt = 0;

        for (let i of p) {
            // https://stackoverflow.com/questions/5584602/determine-timezone-from-latitude-longitude-without-using-web-services-like-geona

            let fuseau_horaire_approx = (i.longitude * 24) / 360;
            let salaire = parseFloat(i.income.slice(1));
            let moyennesalairehomme = this.salairemoyenhomme(p);

            if (
                i.pref_movie.includes("Film-Noir") &&
                i.gender == "Female" &&
                Math.abs(fuseau_horaire_approx) < 1 &&
                salaire < moyennesalairehomme
            ) {
                cpt++;
                result = i.date_of_birth;
                let valueannée = parseFloat(result.slice(0, 4));
                let valuemois = parseFloat(result.slice(5, 7));
                let valuejour = parseFloat(result.slice(8));

                let d = new Date();
                let jour_actuel = d.getUTCDate();
                let mois_actuel = d.getUTCMonth();
                let année_actuel = d.getUTCFullYear();
                if (valuemois < mois_actuel) {
                    age = année_actuel - valueannée;
                } else if (valuemois == mois_actuel && valuejour < jour_actuel) {
                    age = année_actuel - valueannée;
                } else {
                    age = année_actuel - valueannée - 1;
                }

                total += age;
            }
        }

        return total / cpt;
    },

    hommesprochefilm: function (p) {
        let listehlookingforh = [];
        let liste_couple = [];
        //console.log(p[85]);

        for (let i of p) {
            if (i.gender != "Female" && i.looking_for === "M") {
                listehlookingforh.push(i);
                //console.log(i);
            }
        }

        //console.log(listehlookingforh);

        var liste_genre_film = this.liste_genres(p);

        for (let i = 0; i < listehlookingforh.length; i++) {
            for (let j = i + 1; j < listehlookingforh.length; j++) {
                let personne1 = listehlookingforh[i];
                let personne2 = listehlookingforh[j];
                for (let genre of liste_genre_film) {
                    if (
                        personne1.pref_movie.includes(genre) &&
                        personne2.pref_movie.includes(genre)
                    ) {
                        let distance_couple = this.distance(
                            personne1.latitude,
                            personne1.longitude,
                            personne2.latitude,
                            personne2.longitude
                        );
                        let mon_couple = [
                            personne1.last_name,
                            personne2.last_name,
                            distance_couple,
                        ];
                        liste_couple.push(mon_couple);
                        break;
                    }
                }
            }
        }

        let liste_couple_trié = liste_couple.sort(function (a, b) {
            return a[2] - b[2];
        });
        return liste_couple_trié;
    },

    couplefemmehomme: function (p) {
        let liste_couple = [];
        let couple_pas_bon = 0;

        var liste_genre_film = this.liste_genres(p);

        for (let i of p) {
            for (let j of p) {
                couple_pas_bon = 0;

                if (
                    i.gender == "Female" &&
                    i.looking_for === "M" &&
                    j.gender != "Female" &&
                    j.looking_for === "F"
                ) {
                    for (let genre of liste_genre_film) {
                        if (i.pref_movie.includes(genre) && !j.pref_movie.includes(genre)) {
                            couple_pas_bon = 1;
                        } else if (
                            j.pref_movie.includes(genre) &&
                            !i.pref_movie.includes(genre)
                        ) {
                            couple_pas_bon = 1;
                        }
                    }

                    if (couple_pas_bon == 0) {
                        liste_couple.push([i.id, i.last_name, j.id, j.last_name]);
                    }
                }
            }
        }
        return liste_couple;
    },


    
    match: function (p) {
        var liste_genre_film = this.liste_genres(p);

        let liste_couple_bon = [];
        
        
        class couple {
            constructor(
                last_name1,
                id1,
                last_name2,
                id2,
                distance,
                nb_genre_en_commun
            ) {
                this.last_name1 = last_name1;
                this.id1 = id1;
                this.last_name2 = last_name2;
                this.id2 = id2;
                this.distance = distance;
                this.nb_genre_en_commun = nb_genre_en_commun;
            }
        }

        // On parcourt tout les couples possibles
        for (let i of p) {
            for (let j of p) {

                

               

                // pas de couples doublons,  possible optimiser en incluant dans le for du dessus ? 
                if (i.id >= j.id) {
                    continue;
                }


                // tout écrire en 1 seule if ?
                if (i.looking_for === "M" && j.gender === "Female") {
                    continue;
                } else if (i.looking_for === "F" && j.gender === "Male") {
                    continue;
                } else if (j.looking_for === "M" && i.gender === "Female") {
                    continue;
                } else if (j.looking_for === "F" && i.gender === "Male") {
                    continue;
                }



                // 4 verif salaire hetero
                if (i.gender != j.gender) {
                    let salaire1 = parseFloat(i.income.slice(1));
                    let salaire2 = parseFloat(j.income.slice(1));
                    let salairefemme;
                    let salairehomme;
                    if (i.gender == "F") {
                        salairefemme = salaire1;
                        salairehomme = salaire2;
                    } else {
                        salairefemme = salaire2;
                        salairehomme = salaire1;
                    }

                    if (salairefemme < salairehomme / 2) {
                        continue;
                    } else if (salairefemme > salairehomme * 0.9) {
                        continue;
                    }
                }

                // 5 GOOGLE, faire en 1 seule condition ?

                if (i.email.includes("google") && !j.email.includes("google")) {
                    continue;
                } else if (j.email.includes("google") && !i.email.includes("google")) {
                    continue;
                }

                // 6 Likedrama

                if (i.pref_movie.includes("Drama") && !j.pref_movie.includes("Drama")) {
                    continue;
                } else if (
                    j.pref_movie.includes("Drama") &&
                    !i.pref_movie.includes("Drama")
                ) {
                    continue;
                }

                // 7 Likeadventure

                if (
                    i.pref_movie.includes("Adventure") &&
                    !j.pref_movie.includes("Adventure")
                ) {
                    continue;
                } else if (
                    j.pref_movie.includes("Adventure") &&
                    !i.pref_movie.includes("Adventure")
                ) {
                    continue;
                }

                // 8 diff age

                let resulti = i.date_of_birth;
                let année_de_i = parseFloat(resulti.slice(0, 4));
                let mois_de_i = parseFloat(resulti.slice(5, 7));
                let jour_de_i = parseFloat(resulti.slice(8));
                let resultj = j.date_of_birth;
                let année_de_j = parseFloat(resultj.slice(0, 4));
                let mois_de_j = parseFloat(resultj.slice(5, 7));
                let jour_de_j = parseFloat(resultj.slice(8));
                let age1 = this.returnage(année_de_i, mois_de_i, jour_de_i);
                let age2 = this.returnage(année_de_j, mois_de_j, jour_de_j);
                if (age2 < age1 * 0.75 || age1 < age2 * 0.75) {
                    continue;
                }
  


                // A ne calculer que pour les couples qui sont bons
                let distance_couple = this.distance(
                    i.latitude,
                    i.longitude,
                    j.latitude,
                    j.longitude
                );


                 // A ne calculer que pour les couples qui sont bons aussi
                let nb_genre_en_commun = 0;
                for (let genre of liste_genre_film) {
                    if (i.pref_movie.includes(genre) && j.pref_movie.includes(genre)) {
                        nb_genre_en_commun++;
                    }
                }

                
                    let moncouple = new couple(
                        i.last_name,
                        i.id,
                        j.last_name,
                        j.id,
                        distance_couple,
                        nb_genre_en_commun
                    );
                    liste_couple_bon.push(moncouple);
                
            }
        }


        // Tri des couples par distance, puis par genre si distance égal meme si c'est surement jamais le cas sans arrondir
        let liste_couple_bon_trié = liste_couple_bon.sort(function (a, b) {
            if (a.distance != b.distance) {
                return a.distance - b.distance;
            } else return a.nb_genre_en_commun - b.nb_genre_en_commun;
        });
        return liste_couple_bon;
    },
};
